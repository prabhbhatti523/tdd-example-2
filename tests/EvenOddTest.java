import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EvenOddTest {

	@Before
	public void setUp() throws Exception {
	}
	
//	R1: Accepts 1 integer value, n
//	N > 1  
//	R2: If N < 1, then return false
//	R3: If n is even, return true
//	R4: If n is odd, return false


	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEvenOddFunction() {
		EvenOdd e = new EvenOdd();
		assertEquals(false, e.isEven(25));
	}

}
